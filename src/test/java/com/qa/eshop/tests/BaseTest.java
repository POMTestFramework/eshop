package com.qa.eshop.tests;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import com.qa.eShop.factory.DriverFactory;
import com.qa.eShop.pages.CartPage;
import com.qa.eShop.pages.CheckoutPage;
import com.qa.eShop.pages.LoginPage;
import com.qa.eShop.pages.ProductInfoPage;
import com.qa.eShop.pages.LandingPage;
import com.qa.eShop.pages.SearchResultsPage;

public class BaseTest {
	public WebDriver driver;
	public DriverFactory driverFactory;
	public Properties prop;
	public LoginPage loginPage;
	public LandingPage landingPage;
	public SearchResultsPage searchResultsPage;
	public ProductInfoPage productInfoPage;
	public CartPage cartPage;
	public CheckoutPage checkoutPage;
	public SoftAssert softAssert = new SoftAssert();


	@BeforeTest
	public void setup() {
		driverFactory = new DriverFactory();
		prop = driverFactory.init_prop();
		driver = driverFactory.init_driver(prop);
		loginPage = new LoginPage(driver);
	}

	@AfterTest
	public void tearDown() {
		driver.quit();

	}

}
