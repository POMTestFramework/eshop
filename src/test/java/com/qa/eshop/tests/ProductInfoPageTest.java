package com.qa.eshop.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.eShop.Utils.Constants;

public class ProductInfoPageTest extends BaseTest {
	public String url;

	@BeforeClass
	public void productPageSetup() {
		landingPage = loginPage.doLogin(prop.getProperty("username").trim(), prop.getProperty("password").trim());
	}

	@DataProvider
	public Object[][] productData() {
		return new Object[][] { { "mac", "macBook Pro", " MacBook Pro description", "1000 euros", "2" } };
	}

	@Test(dataProvider = "productData", description = "Verifies the Product details-> adds the product to cart->Checks if the right product is added to the cart")
	public void addProductToCartTest(String productName, String mainProductName, String description, String price,
			String quantity) {
		searchResultsPage = landingPage.doSearch(productName);
		productInfoPage = searchResultsPage.selectProduct(mainProductName);
		url = driver.getCurrentUrl();
		productInfoPage.setQuantity(quantity);
		Assert.assertEquals(productInfoPage.getProductData("Product Name"), mainProductName);
		Assert.assertEquals(productInfoPage.getProductData("Product Description"), description);
		Assert.assertEquals(productInfoPage.getProductData("Product Price"), price);
		Assert.assertTrue(productInfoPage.isDisplayed("Product Image"));
		Assert.assertTrue(productInfoPage.isDisplayed("Add To Cart"));
		String sucessMessage = productInfoPage.addToCart();
		Assert.assertEquals(sucessMessage, Constants.ADD_TO_CART_SUCCESSMSG);
		cartPage = productInfoPage.navigateToCart();
		productInfoPage = cartPage.selectProduct(mainProductName);
		Assert.assertEquals(url, driver.getCurrentUrl());

	}

}
