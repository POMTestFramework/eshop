package com.qa.eshop.tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.eShop.Utils.Constants;

public class LoginPageTest extends BaseTest {

	@DataProvider
	public Object[][] InvalidCredentials() {
		return new Object[][] { { "", "" }, { "ValidUser", "" }, { "", "ValidPwd" }, { "InvalidUser", "" },
				{ "", "InvalidPwd" }, { "InvalidUser", "InvalidPwd" }, { "ValidUser", "InvalidPwd" },
				{ "InvalidUser", "ValidPwd" } };
	}

	@Test(description = "Validates the UI elements")
	public void checkUI() {
		Assert.assertEquals(loginPage.isPresent("Username"), true, "Username Textbox is not displayed");
		Assert.assertEquals(loginPage.isPresent("Password"), true, "Password Textbox is not displayed");
		Assert.assertEquals(loginPage.isPresent("Login Button"), true, "Login Button is not  displayed");
		Assert.assertEquals(loginPage.isPresent("Cancel Button"), true, "Cancel Button is not displayed");
	}

	@Test(description = "Verifies if login in succesfull with valid Credentials")
	public void validLoginTest() {
		landingPage = loginPage.doLogin(prop.getProperty("username").trim(), prop.getProperty("password").trim());
		Assert.assertEquals(landingPage.productPageTitleTest(), Constants.LANDING_PAGE_TITLE);
		Assert.assertEquals(landingPage.getLoginSuccessMsg(), Constants.LOGIN_SUCCESS_MESSAGE);
	}

	@Test(dataProvider = "InvalidCredentials",description = "Verifies if error messafe is dispalyed , when login is attempted with"
			+ "  ivalid Credentials")
	public void invalidLoginTest(String username, String password) {
		String url = driver.getCurrentUrl();
		loginPage.doInvalidLogin(username.trim(), password.trim());
		Assert.assertEquals(driver.getCurrentUrl(), url);
		Assert.assertTrue(loginPage.isPresent("Login Button"));
		Assert.assertTrue(loginPage.checkIfUnameEmpty());
		Assert.assertTrue(loginPage.checkIfPwdEmpty());
		Assert.assertEquals(loginPage.checkFailureMsg(), Constants.LOGIN_FAILURE_MESSAGE);
	}

	@Test(description= "Verifies if the Username & Password fields are cleared , When valid credentials are entered and cancel button is clicked")
	public void cancelValidlogin() {

		String url = driver.getCurrentUrl();
		loginPage.doCancel(prop.getProperty("username").trim(), prop.getProperty("password").trim());
		Assert.assertEquals(driver.getCurrentUrl(), url);
		Assert.assertTrue(loginPage.isPresent("Login Button"));
		Assert.assertTrue(loginPage.checkIfUnameEmpty());
		Assert.assertTrue(loginPage.checkIfPwdEmpty());
	}

	@Test(dataProvider = "InvalidCredentails" ,description= "Verifies if the Username & Password fields are cleared , When invalid credentials are entered and cancel button is clicked ")
	public void cancelValidLogin(String username, String password) {
		String url = driver.getCurrentUrl();
		loginPage.doCancel(username.trim(), password.trim());
		Assert.assertEquals(driver.getCurrentUrl(), url);
		Assert.assertTrue(loginPage.isPresent("Login Button"));
		Assert.assertTrue(loginPage.checkIfUnameEmpty());
		Assert.assertTrue(loginPage.checkIfPwdEmpty());
	}

}
