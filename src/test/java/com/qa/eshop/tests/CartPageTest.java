package com.qa.eshop.tests;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.junit.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.eShop.Utils.Constants;
import com.qa.eShop.Utils.XMLParser;
import com.qa.eShop.Utils.propertyReader;

public class CartPageTest extends BaseTest {
	public int id;
	public int orderNumber;
	public String product_Id;
	public int quantity;
	public String productName;

	// Connection object
	static Connection con = null;
	// Statement object
	private static Statement stmt;

	propertyReader propRead = new propertyReader("orderInfo");
	HashMap<String, String> xpathasMap = propRead.getPropertyAsHashMap();

	@BeforeClass
	public void cartPageSetup() throws Exception {
		landingPage = loginPage.doLogin(prop.getProperty("username").trim(), prop.getProperty("password").trim());
		landingPage.navigateToCart();
		setUpDb();
	}

	@DataProvider
	public Object[][] productNolongerAvailable() {
		return new Object[][] { { "hp", "hp notebook", " yogaNoteBook", "800 euros", "1" } };
	}

	@Test(dataProvider = "productData", description= "end-end scenario from selecting product->add to cart->checkout")

	public void checkoutSucess(String productName, String mainProductName, String description, String price,
			int quantity) {
		this.quantity = quantity;
		searchResultsPage = landingPage.doSearch(productName);
		productInfoPage = searchResultsPage.selectProduct(mainProductName);
		this.productName = mainProductName;
		product_Id = productInfoPage.getProductData("productId");
		productInfoPage.addToCart();
		cartPage = productInfoPage.navigateToCart();
		checkoutPage = cartPage.clickCheckOut();
		Assert.assertEquals(checkoutPage.checkUserName(), prop.getProperty("username"));
		Assert.assertEquals(checkoutPage.checkSuccessMsg(), Constants.CHECKOUT_SUCCESS_MESSAGE);
		orderNumber = Integer.parseInt(checkoutPage.getOrderNumber());

	}

	@Test(dependsOnMethods = { "checkoutSuccess" }, description ="Once the checkout testcase is successful, this testcase verifies if the order details are saved in database correctly")
	public void verifyOrderInDatabase() {
		
		
		try {
			int order_ID = 0;
			String product_ID=null;
			int qty=0;
		
			String query = "select * from Order_Line_Details where order_id =(select ID from Order_Data where order_Number = "
					+ this.orderNumber + "" + "&& customer_id = " + "(select id from customer_Data where username="
					+ prop.getProperty("username") + ";";

			// Get the contents of userinfo table from DB
			ResultSet res = stmt.executeQuery(query);

			// res.next() returns true if there is any next record else returns false
			while (res.next()) {
				this.id = res.getInt("ID");
				order_ID = res.getInt("Order_ID");
				product_ID = res.getString("Product_ID");
				qty = res.getInt("Quantity");

			}
			Assert.assertEquals(this.orderNumber, order_ID);
			Assert.assertEquals(this.product_Id, product_ID);
			Assert.assertEquals(this.quantity, qty);
		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test(dependsOnMethods = { "checkoutSuccess" },description ="Once the checkout testcase is successful, this testcase checks if the  order details accurate in the XML generated")
	public void verifyOrderInXML() {
		softAssert.assertEquals(id, XMLParser.getXPathValue(xpathasMap.get("lineID")));
		softAssert.assertEquals(this.orderNumber,
				Integer.parseInt(XMLParser.getXPathValue(xpathasMap.get("orderNumber"))));
		softAssert.assertEquals(this.product_Id, XMLParser.getXPathValue(xpathasMap.get("product_Id")));
		softAssert.assertEquals(this.productName, XMLParser.getXPathValue(xpathasMap.get("productName")));
		softAssert.assertEquals(this.quantity, XMLParser.getXPathValue(xpathasMap.get("quantity")));
		softAssert.assertEquals(prop.getProperty("username"), XMLParser.getXPathValue(xpathasMap.get("username")));
		softAssert.assertAll();

	}

	@Test(dataProvider = "productNolongerAvailable", description=" Checks if the checkout is unsuccessful when a product is added to the cart and checkout button is clicked")

	public void checkoutFailure(String productName, String mainProductName, String description, String price) {
		searchResultsPage = landingPage.doSearch(productName);
		productInfoPage = searchResultsPage.selectProduct(mainProductName);
		productInfoPage.addToCart();
		cartPage = productInfoPage.navigateToCart();
		checkoutPage = cartPage.clickCheckOut();
		Assert.assertEquals(cartPage.checkCheckOutFailure(), Constants.CHECKOUT_FAILURE_MESSAGE);

	}

	// Database Connection and Disconnection

	/*
	 * Establishing the Database connection
	 */
	public void setUpDb() throws Exception {
		try {
			// Make the database connection
			String dbClass = "com.mysql.jdbc.Driver";
			Class.forName(dbClass).newInstance();
			// Get connection to DB
			Connection con = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER, Constants.DB_PASSWORD);
			// Statement object to send the SQL statement to the Database
			stmt = con.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Tearing down the database connection.
	 */

	@AfterClass
	public void tearDownDb() throws SQLException {
		// Close DB connection
		if (con != null) {
			con.close();
		}
	}

}
