package com.qa.eShop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.qa.eShop.Utils.ElementUtil;

public class ProductInfoPage {
	private WebDriver driver;
	private ElementUtil elementUtil;

	// By Locator

	private By productName = By.cssSelector("<TBD>");
	private By productImage = By.cssSelector("<TBD>");
	private By productDescription = By.cssSelector("<TBD>");
	private By productPrice = By.cssSelector("<TBD>");
	private By productId = By.cssSelector("<TBD>");
	private By quantity = By.id("<TBD>");
	private By addToCart = By.id("<TBD>");
	private By successMessage = By.cssSelector("TBD");
	private By cart = By.linkText("<TBD>");


	// Constructor
	public ProductInfoPage(WebDriver driver) {
		this.driver = driver;
		elementUtil = new ElementUtil(driver);

	}

	// Page Methods
	
	/*
	 * Extracts the Product related information for the given keyword
	 */
	
	public String getProductData(String locator) {
		switch (locator) {

		case "Product Name":
			return elementUtil.doGetText(productName);

		case "Product Description":
			return elementUtil.doGetText(productDescription);
		case "Product Price":
			return elementUtil.doGetText(productPrice);
		case "Product Id":
			return elementUtil.doGetText(productId);

		}
		return null;

	}

	/*
	 * Checks for the presence of the Webelement
	 */
	public boolean isDisplayed(String locator) {
		switch (locator) {

		case "Product Image":
			return elementUtil.doIsDisplayed(productImage);

		case "Add To Cart":
			return elementUtil.doIsDisplayed(addToCart);

		}
		return false;

	}
	
	/*
	 * Clicks on the Add To Cart button
	 */
	
	public  String addToCart() {
		elementUtil.doClick(addToCart);
		return elementUtil.doGetText(successMessage);
		
	}
	
	/*
	 * Clicks on the Cart Webelement
	 */
	public CartPage navigateToCart() {

		elementUtil.doClick(cart);
		return new CartPage(driver);
	}
	
	/*
	 *  Passes value into the quantity field
	 */

	public void setQuantity(String pQuantity) {
         elementUtil.doSendKeys(quantity, pQuantity);		
	}

}
