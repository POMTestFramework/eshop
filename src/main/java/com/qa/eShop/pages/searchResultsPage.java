package com.qa.eShop.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.qa.eShop.Utils.ElementUtil;

public class SearchResultsPage {

	private WebDriver driver;
	private ElementUtil elementUtil;

	// By Locator

	private By productResults = By.cssSelector("div.caption a");


	// Constructor
	public SearchResultsPage(WebDriver driver) {
		this.driver = driver;
		elementUtil = new ElementUtil(driver);
	}

	// Page actions

/*
 * Collects the list of Webelements and identifies and selects the right product
 */
	
	public ProductInfoPage selectProduct(String mainProductName) {
		System.out.println("main product name is : " + mainProductName);
		List<WebElement> searchList = elementUtil.waitForElementsToBeVisible(productResults, 5);
		for (WebElement e : searchList) {
			String text = e.getText();
			if (text.equals(mainProductName)) {
				e.click();
				break;

			}
		}
		return new ProductInfoPage(driver);

	}

}
