package com.qa.eShop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.qa.eShop.Utils.ElementUtil;

public class LoginPage {
	private WebDriver driver;
	private ElementUtil elementUtil;

	// 1. By Locator

	private By emailId = By.id("<TBD>");
	private By password = By.id("<TBD>");
	private By loginBtn = By.xpath("<TBD>");
	private By cancelBtn = By.linkText("<TBD");
	private By errorMsg = By.cssSelector("<TBD>");

	// 2. Constructor
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		elementUtil = new ElementUtil(driver);

	}

	// 3. Page actions
	
	/*
	 * Checks if the Webelement is displayed
	 */
	public boolean isPresent(String locator) {
		switch (locator) {

		case "Username":
			return elementUtil.doIsDisplayed(emailId);
		case "Password":
			return elementUtil.doIsDisplayed(password);
		case "Login Button":
			return elementUtil.doIsDisplayed(loginBtn);
		case "cancel Button":
			return elementUtil.doIsDisplayed(cancelBtn);

		}
		return false;

	}

	/*
	 * Checks if the Username textbox is empty
	 */

	public boolean checkIfUnameEmpty() {
		return elementUtil.getElement(emailId).getAttribute("value").isEmpty();
	}

	/*
	 * Checks if the Password textbox is empty
	 */
	public boolean checkIfPwdEmpty() {
		return elementUtil.getElement(password).getAttribute("value").isEmpty();
	}

	/*
	 * Checks for the Invalid Credentials Error Message
	 */
	public String checkFailureMsg() {
		return elementUtil.getElement(errorMsg).getText();
	}

	/*
	 * Inputs Username and Password and click on login button
	 */
	public LandingPage doLogin(String un, String pwd) {
		System.out.println("Login with:" + un + " : " + pwd);
		elementUtil.doSendKeys(emailId, un);
		elementUtil.doSendKeys(password, pwd);
		elementUtil.doClick(loginBtn);
		return new LandingPage(driver);

	}

	/*
	 * Inputs Invalid Credentials and clicks on login button
	 */
	public void doInvalidLogin(String un, String pwd) {
		System.out.println("Login with:" + un + " : " + pwd);
		elementUtil.doSendKeys(emailId, un);
		elementUtil.doSendKeys(password, pwd);
		elementUtil.doClick(loginBtn);

	}

	/*
	 * Inputs Credentails and clicks on cancel button
	 */

	public void doCancel(String un, String pwd) {
		elementUtil.doSendKeys(emailId, un);
		elementUtil.doSendKeys(password, pwd);
		elementUtil.doClick(cancelBtn);

	}

}
