package com.qa.eShop.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.qa.eShop.Utils.ElementUtil;

public class CartPage {

	private WebDriver driver;
	private ElementUtil elementUtil;

	// 1. By Locator

	private By checkoutBtn = By.id("<TBD");
	private By checkoutFailureMsg = By.cssSelector("<TBD>");
	private By productResults = By.cssSelector("<TBD>");
	private By cart = By.linkText("<TBD>");

	// 2. Constructor
	public CartPage(WebDriver driver) {
		this.driver = driver;
		elementUtil = new ElementUtil(driver);

	}

	// Page methods

	/*
	 * Clicks on the Cart WebElement
	 */
	public CartPage navigateToCart() {
		elementUtil.doClick(cart);
		return new CartPage(driver);
	}

	/*
	 * Collects the list of Webelements and identifies and selects the right product
	 */
	public ProductInfoPage selectProduct(String mainProductName) {
		System.out.println("main product name is : " + mainProductName);
		List<WebElement> searchList = elementUtil.waitForElementsToBeVisible(productResults, 5);
		for (WebElement e : searchList) {
			String text = e.getText();
			if (text.equals(mainProductName)) {
				e.click();
				break;

			}
		}
		return new ProductInfoPage(driver);

	}
	
	/*
	 * Clicks on the Checkout button
	 */

	public CheckoutPage clickCheckOut() {
		elementUtil.doClick(checkoutBtn);
		return new CheckoutPage(driver);

	}
	
	/*
	 * Checks the Checkout Failure meesage
	 */

	public String checkCheckOutFailure() {
		return elementUtil.doGetText(checkoutFailureMsg);

	}

}
