package com.qa.eShop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.qa.eShop.Utils.ElementUtil;

public class CheckoutPage {

	private WebDriver driver;
	private ElementUtil elementUtil;

	// Locators

	private By userName = By.id("<TBD>");
	private By checkOutSuccessMsg = By.xpath("<TBD>");
	private By orderNumber = By.xpath("<TBD>");

	// Constructor
	public CheckoutPage(WebDriver driver) {
		this.driver = driver;
		ElementUtil elementUtil = new ElementUtil(driver);

	}

	// Page actions
	
	/*
	 * Fetches the username
	 */

	public String checkUserName() {
		return elementUtil.getElement(userName).getText();

	}

	/*
	 * Checks the Checkout success meesage
	 */
	public String checkSuccessMsg() {
		return elementUtil.getElement(checkOutSuccessMsg).getText();

	}

	/*
	 * Fetches the Order Number
	 */
	public String getOrderNumber() {
		return elementUtil.doGetText(orderNumber);
	}

}
