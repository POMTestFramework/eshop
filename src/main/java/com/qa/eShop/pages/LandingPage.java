package com.qa.eShop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.qa.eShop.Utils.Constants;
import com.qa.eShop.Utils.ElementUtil;

public class LandingPage {
	private WebDriver driver;
	private ElementUtil elementUtil;

	// By Locator

	private By successMessage = By.cssSelector("TBD");
	private By searchBox = By.cssSelector("<TBD>");
	private By searchButton = By.cssSelector("<TBD>");
	private By cart = By.linkText("<TBD>");

	//Constructor
	public LandingPage(WebDriver driver) {
		this.driver = driver;
		elementUtil = new ElementUtil(driver);

	}

	// Page actions
	
	/*
	 * Checks for the page title
	 */

	public String productPageTitleTest() {
		return elementUtil.waitForTitleIs(Constants.LANDING_PAGE_TITLE, 5);
	}
	
	/*
	 * Checks for the login sucess message
	 */

	public String getLoginSuccessMsg() {
		return elementUtil.doGetText(successMessage);
	}

	/*
	 * Checks if the Search box is displayed
	 */
	public boolean isSearchExists() {
		return elementUtil.doIsDisplayed(searchBox);
	}

	/*
	 * Enters a value and clicks on the Search button
	 */
	public SearchResultsPage doSearch(String productName) {
		System.out.println("Searching for the Product: " + productName);
		elementUtil.doSendKeys(searchBox, productName);
		elementUtil.doClick(searchButton);
		return new SearchResultsPage(driver);
	}

	/* 
	 * Clicks on the Cart WebElement
	 */
	public CartPage navigateToCart() {

		elementUtil.doClick(cart);
		return new CartPage(driver);
	}

}
