package com.qa.eShop.Utils;

import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.io.File;

public class XMLParser {
	public static String getXPathValue(String expression) {
		String filePath = "./xml/order.xml";
		File file = new File(filePath);
		DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			doc.getDocumentElement().normalize();
			
			XPath xpath = XPathFactory.newInstance().newXPath();
			String xpath1 = expression;
			String result = xpath.compile(xpath1).evaluate(doc);
			return result;
		} catch (Exception e1) {
			e1.printStackTrace();
			return null;
		}
	}
}
