package com.qa.eShop.Utils;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

    public class propertyReader {
        private String propertyName = null;
        private Properties props;
       
        public propertyReader(String propertyName) {
            this.propertyName = propertyName;
        }
         private void loadProperty() {
            try {
                props = new Properties();
                FileInputStream fis = new FileInputStream("./xml/"+propertyName+".properties");
                props.load(fis);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public HashMap<String, String> getPropertyAsHashMap() {
            loadProperty();
            HashMap<String, String> map = new HashMap<String, String>();
            for (Entry<Object, Object> entry : props.entrySet()) {
                map.put((String) entry.getKey(), (String) entry.getValue());
            }
            return map;
        }
    }
