package com.qa.eShop.Utils;

public class Constants {

	public static final String DB_URL = "jdbc:mysql://localhost:3306/user";
	public static final String DB_USER = "root";
	public static final String DB_PASSWORD = "root";

	public static final String LOGIN_PAGE_TITLE = "<Login Page Title>";
	public static final String LOGIN_SUCCESS_MESSAGE = "User Logged in Successfully";
	public static final String LOGIN_FAILURE_MESSAGE = "Invalid Credentials";
	public static final String LANDING_PAGE_TITLE = "<Products page Tile> ";

	public  static final String ADD_TO_CART_SUCCESSMSG = "<TBD>";

	public static final String CHECKOUT_SUCCESS_MESSAGE = "Checkout Successful";
	public static final String CHECKOUT_FAILURE_MESSAGE = "Checkout Failed";
	

}
